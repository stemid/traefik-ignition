variable "settings" {
  type = object({
    logLevel = string
    redirect_http_to_https = bool
    default_certresolver = string
    entrypoints = list(object({
      name = string
      address = string
    }))
    enable_forwarded_headers = bool
    enable_api = bool
    api_listen_address = string
  })
  default = {
    logLevel = "ERROR"
    redirect_http_to_https = true
    default_certresolver = "letsencrypt"
    entrypoints = []
    enable_forwarded_headers = false
    enable_api = false
    api_listen_address = "127.0.0.1:8080"
  }
}

variable "acme" {
  type = list(object({
    name = optional(string, "letsencrypt")
    email = string
    caserver = optional(
      string,
      "https://acme-staging-v02.api.letsencrypt.org/directory"
    )
    certificatesDuration = optional(number, 2160) # 90 days

    eab = optional(object({
      kid = optional(string, "")
      hmacEncoded = optional(string, "")
    }), {})

    httpChallenge = optional(object({
      entryPoint = optional(string, "")
    }), {})

    dnsChallenge = optional(object({
      provider = optional(string, "")
    }), {})
  }))
}

variable "http_services" {
  type = list(object({
    name = string
    servers = list(object({
      url = string
    }))
  }))
  default = []
}

variable "http_routers" {
  type = list(object({
    name = string
    rule = string
    service = string
    entryPoints = string
    priority = optional(number, 0)
    certResolver = string
    middlewares = optional(list(string), [])
  }))
  default = []
}

variable "http_middlewares" {
  type = list(object({
    name = string
    customResponseHeaders = optional(list(object({
      name = string
      value=string
    })), [])
  }))
  default = []
}

variable "tcp_services" {
  type = list(object({
    name = string
    addresses = list(string)
  }))
  default = []
}

variable "tcp_routers" {
  type = list(object({
    name = string
    rule = string
    entryPoints = string
    service = string
  }))
  default = []
}

variable "aws_access_key_id" {
  type = string
  default = ""
}

variable "aws_secret_access_key" {
  type = string
  default = ""
}
