data "ignition_link" "backup_timer" {
  path = "/etc/systemd/system/timers.target.wants/podman-volume-backup@systemd-acme.timer"
  target = "/etc/systemd/system/podman-volume-backup@.timer"
  hard = false
}

data "ignition_systemd_unit" "restore_unit" {
  name = "podman-volume-restore@systemd-acme.service"
  enabled = true
}

data "ignition_systemd_unit" "backup_timer" {
  name = "podman-volume-backup@systemd-acme.timer"
  enabled = true
}

data "ignition_directory" "traefik" {
  path = "/var/opt/traefik"
  mode = 448
}

data "ignition_directory" "acme" {
  count = length(var.acme)
  path = "/var/opt/traefik/${var.acme[count.index].name}"
  mode = 448
}

data "ignition_file" "acme" {
  path = "/var/opt/traefik/letsencrypt/acme.json"
  content {
    content = ""
  }
  mode = 384
}

data "ignition_file" "environment" {
  path = "/var/opt/traefik/environment"
  content {
    content = templatefile("${path.module}/templates/environment", {
      aws_access_key_id = var.aws_access_key_id,
      aws_secret_access_key = var.aws_secret_access_key
    })
  }
  mode = 384
}

data "ignition_file" "config" {
  path = "/var/opt/traefik/traefik.toml"
  content {
    content = templatefile("${path.module}/templates/traefik.toml", {
      acme = var.acme
      conf = var.settings
    })
  }
  mode = 384
}

data "ignition_file" "dynamic_config" {
  path = "/var/opt/traefik/dynamic.toml"
  content {
    content = templatefile("${path.module}/templates/dynamic.toml", {
      http_services = var.http_services
      http_routers = var.http_routers
      http_middlewares = var.http_middlewares
      tcp_services = var.tcp_services
      tcp_routers = var.tcp_routers
    })
  }
  mode = 384
}

data "ignition_file" "container_unit" {
  path = "/etc/containers/systemd/traefik.container"
  content {
    content = templatefile("${path.module}/templates/traefik.container", {
      conf = var.settings
    })
  }
}

data "ignition_file" "volume_unit" {
  path = "/etc/containers/systemd/acme.volume"
  content {
    content = file("${path.module}/templates/acme.volume")
  }
}

data "ignition_file" "sysctl" {
  path = "/etc/sysctl.d/90-rmem.conf"
  content {
    content = "net.core.rmem_max=2097152\nnet.core.wmem_max=2097152"
  }
}

data "ignition_config" "config" {
  links = [
    data.ignition_link.backup_timer.rendered,
  ]

  systemd = [
    data.ignition_systemd_unit.backup_timer.rendered,
    data.ignition_systemd_unit.restore_unit.rendered
  ]

  directories = concat([
    data.ignition_directory.traefik.rendered,
  ], [for d in data.ignition_directory.acme : d.rendered])

  files = [
    data.ignition_file.acme.rendered,
    data.ignition_file.environment.rendered,
    data.ignition_file.config.rendered,
    data.ignition_file.dynamic_config.rendered,
    data.ignition_file.container_unit.rendered,
    data.ignition_file.volume_unit.rendered,
    data.ignition_file.sysctl.rendered,
  ]
}
