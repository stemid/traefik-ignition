# Traefik

Deploys Traefik with ignition on CoreOS.

This module depends on [podman-volume-restore](https://gitlab.com/stemid/podman-volume-restore-ignition.git), to save and restore acme.json on S3.

Although this is a podman quadlet, it only supports deployment as root right now.
